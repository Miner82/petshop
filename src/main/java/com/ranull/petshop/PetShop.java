package com.ranull.petshop;

import com.ranull.petshop.commands.PetShopCommand;
import com.ranull.petshop.hooks.Vault;
import com.ranull.petshop.listeners.PlayerInteractEntityListener;
import com.ranull.petshop.manager.PetManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class PetShop extends JavaPlugin {

    @Override
    public void onEnable() {
        saveDefaultConfig();

        // Delayed to ensure vault is initialised by the time we try to call it
        Bukkit.getScheduler().runTaskLater(this, this::setupPlugin, 5);
    }

    @Override
    public void onDisable() {
        Bukkit.getServer().getScheduler().cancelTasks(this);
    }

    private boolean setupPlugin() {

        Vault vault = new Vault();

        if (!vault.setupEconomy()) {
            System.out.println(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return false;
        }

        // Plugin startup logic
        getCommand("petshop").setExecutor(new PetShopCommand(this));
        getServer().getPluginManager().registerEvents(new PlayerInteractEntityListener(this,
                new PetManager(this, vault.getEconomy())), this);

        return true;

    }

}