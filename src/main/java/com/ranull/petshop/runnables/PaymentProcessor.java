package com.ranull.petshop.runnables;

import com.ranull.petshop.PetShop;
import com.ranull.petshop.manager.PetManager;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class PaymentProcessor extends BukkitRunnable {

    private PetShop plugin;
    private PetManager manager;
    private Economy economy;
    private UUID recipient;
    private Player purchaser;
    private Double paymentAmount;
    private Tameable purchasedPet;

    public PaymentProcessor(PetShop plugin, PetManager manager, Economy economy, UUID recipient, Player purchaser, Double paymentAmount, Tameable pet) {

        this.plugin = plugin;
        this.manager = manager;
        this.economy  = economy;
        this.recipient = recipient;
        this.purchaser = purchaser;
        this.paymentAmount = paymentAmount;
        this.purchasedPet = pet;

    }

    @Override
    public void run() {

        final double payment = paymentAmount;

        try {

            if(economy.has(purchaser, payment)) {

                EconomyResponse withdrawResponse = economy.withdrawPlayer(purchaser, payment);

                if(withdrawResponse.transactionSuccess()) {

                    EconomyResponse depositResponse = economy.depositPlayer(purchaser.getServer().getOfflinePlayer(recipient), payment);

                    if(depositResponse.transactionSuccess()) {

                        manager.transferPetOwnership(recipient, purchaser, purchasedPet, payment);

                    }
                    else {

                        purchaser.sendMessage(ChatColor.RED + plugin.getConfig().getString("settings.paymentErrorContactOP") + withdrawResponse.errorMessage);

                        if(Bukkit.getPlayer(recipient) != null) {

                            Bukkit.getPlayer(recipient).sendMessage(ChatColor.RED + plugin.getConfig().getString("settings.paymentErrorContactOP") + withdrawResponse.errorMessage);

                        }

                    }

                }
                else {

                    purchaser.sendMessage(ChatColor.RED + plugin.getConfig().getString("settings.paymentError") + withdrawResponse.errorMessage);

                }

            }
            else {

                String notEnoughMoney = plugin.getConfig().getString("settings.notEnoughMoney")
                        .replace("&", "§");

                purchaser.sendMessage(notEnoughMoney);

            }

        }
        catch (Exception e) {
            System.out.println("An error occurred while attempting a PetShop purchase:");
            e.printStackTrace();
        }

    }
}
